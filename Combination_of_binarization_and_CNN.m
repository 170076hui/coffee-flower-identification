clc;clear;close all
%%
%load the trained CNN network
net=load('...') ;
net = vl_simplenn_move(net, 'gpu') ;
net = vl_simplenn_tidy(net) ;
%%
%The identification process of CNN network 
num1=1080;
num2=1920;
OriginalImage=imread('...');
input1=zeros(1120,1960,3);
input1(21:1100,21:1940,:)=OriginalImage;
win=15;
floor=0;
 for i=1:num1
          for j=1:num2
              floor=floor+1
               tem=input1(i+20-win:i+20+win, j+20-win:j+20+win, :);
                 im_=single(tem);
                 im_ = imresize(im_, net.meta.normalization.imageSize(1:2)) ;
 
                 im_(:,:,1)=  im_(:,:,1)-net.meta.normalization.averageImage(1);
                 im_(:,:,2)=  im_(:,:,2)-net.meta.normalization.averageImage(2);
                 im_(:,:,3)=  im_(:,:,3)-net.meta.normalization.averageImage(3);
                im_ = gpuArray(im_);
                 res = vl_simplenn(net, im_) ;
                 scores = squeeze(gather(res(end).x)) ;
                [bestScore, best] = max(scores) ;
                 pre_best(floor) = best;
          
      end
     
 end
predict_label=reshape(pre_best,1920,1080)';
Precdict_Label=abs(predict_label-3);
%%
%Binarization segmentation based on the nearest threshold
result1=graythresh(OriginalImage);
Result=double(im2bw(OriginalImage,result1+0));
Result=Result+1;
%%
%The results of CNN are used to remove the background information left in the binarization result
num1=1080;
num2=1920;
for i=1:num1
    for j=1:num2
       if Precdict_Label(i,j)==1           
        Result(i,j)=1;
       end     
    end
end
figure;
imagesc(Result)