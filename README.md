**Coffee flower identification using binarization algorithm based on convolutional neural network for digital images**


## CropImages

Croped training data sets with size of 31*31, and the corresponding labels are described in the txt file named ImageLabel



---

## OriginalImages

(1)Negative:12 digital images without coffee flowers;
(2)Positive: There are 40 digital pictures containing coffee flowers, and the extracted coffee flower coordinate points are saved in the file named gtindex;
(3)Test: 5 test digital images with different depression angles and illumination conditions, the corresponding truth maps are saved in the file named testgt.

---
## code

Code of the proposed method

---